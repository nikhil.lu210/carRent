<?php

namespace App\Http\Controllers\Admin\Vehicle;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Vahical;
use App\Type;
use Illuminate\Support\Facades\File;

class VehiclesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehicles = Vahical::all();
        return view('vehicles.index')->withVehicles($vehicles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = Type::all();
        return view('vehicles.create')->withTypes($types);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,array(
            'name'         =>  'required|string|max:191',
            'seat_no'        =>  'required|integer',
            'cost_1_way'        =>  'required|integer',
            'cost_2_way'        =>  'required|integer',
            'reg_no'          =>  'required|string',
        ));

        if($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $mime = $avatar->getMimeType();

            if ($mime == "image/jpeg" || $mime == "image/png" || $mime == "image/svg+xml") {
                $this->validate($request,array(
                    'avatar'    =>  'image|mimes:jpeg,png,jpg,svg|max:1024',
                ));
                                
            }          
        }

        $vehicle = new Vahical();

        $vehicle->type_id = $request->type_id;
        $vehicle->name = $request->name;
        $vehicle->seat_no = $request->seat_no;
        $vehicle->perdat_cost_1_way = $request->cost_1_way;
        $vehicle->perdat_cost_2_way = $request->cost_2_way;
        $vehicle->reg_no = $request->reg_no;

        // if avatar is present
        if($request->hasFile('avatar')){
            $path = time().'.'.$request->avatar->getClientOriginalExtension();
            $request->avatar->move(public_path('vehicle_image'), $path);
        
            $vehicle->avatar = $path;
        }

        $vehicle->save();

        return redirect()->route('admin.vehicle');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('vehicles.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vehicle = Vahical::find($id);

        $image_path = public_path().'\vehicle_image\\'.$vehicle->avatar;
        // dd($image_path);
        if(File::exists($image_path))
        {
            File::delete($image_path);
            // unlink($image_path);
        }

        $vehicle->delete();

        return redirect()->back();
    }
}
