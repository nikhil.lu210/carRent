<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Order;
use Mail;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $this->validate($request, array(
            'journey_date'      =>  'max:50',
            'user_name'      =>  'string|max:50',
            'mobile'      =>  'string|max:50',
            'note'      =>  'string|max:200',
            'needed_vahicle'      =>  'integer|max:50',
            'pickPoint'      =>  'string|max:50',
            'dropPoint'      =>  'string|max:50',
            'way'      =>  'integer|max:50'
        ));

        $order = new Order();

        $order->order_day = $request->journey_date;
        $order->user_name = $request->user_name;
        $order->type_id = $id;
        $order->mobile = $request->mobile;
        $order->address = $request->note;
        $order->needed_vahical = $request->needed_vahicle;
        $order->pickPoint = $request->pickPoint;
        $order->dropPoint = $request->dropPoint;
        $order->way = $request->way;
        $order->total = ($request->way == 1 ) ? $request->way1cost*$request->needed_vahicle:$request->way2cost*$request->needed_vahicle;

        $order->save();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    
}
