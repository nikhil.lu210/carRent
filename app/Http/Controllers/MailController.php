<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Mail;

class MailController extends Controller
{
    //mail for mail
    public function mail(Request $request){

        $data = [
            'subject' => 'for ordering information',
    		'name'	 =>	$request->name,
			'email'	 => $request->email,
			'msg' => $request->msg,
    	];

        Mail::send('mails.contact', $data, function($message) use ($data) {
			$message->from($data['email']);
			$message->subject($data['subject']);
			$message->to('mahbubtajkia.lu@gmail.com');
       });
       
       return redirect()->back();
    }
}
