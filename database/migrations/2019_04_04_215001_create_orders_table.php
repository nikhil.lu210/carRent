<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_day');

            $table->string('user_name');
            $table->integer('type_id');
            $table->string('mobile');
            $table->string('address');
            $table->integer('needed_vahical');
            $table->string('pickPoint');
            $table->string('dropPoint');
            $table->integer('way');
            $table->double('total', 8, 2);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
