<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', [
	'uses' => 'FrontPagesController@index',
	'as' => 'welcome'
]);

/* =======> Front Page Part <======= */
Route::get('/frontpages', [
	'uses' => 'FrontPagesController@index',
	'as' => 'frontpages'
]);
Route::get('/frontpage/create', [
	'uses' => 'FrontPagesController@create',
	'as' => 'frontpage.create'
]);
Route::post('/frontpage/store', [
	'uses' => 'FrontPagesController@store',
	'as' => 'frontpage.store'
]);
Route::get('/frontpage/show/{id}', [
	'uses' => 'FrontPagesController@show',
	'as' => 'frontpage.show'
]);

Route::post('/frontpage/update/{id}', [
	'uses' => 'FrontPagesController@update',
	'as' => 'frontpage.update'
]);

/* =======> Front Page Part <======= */

Auth::routes();

// admin dashboard
Route::get('/home', 'HomeController@index')->name('home');

// admin order section
Route::group(['prefix' => 'admin/orders', 'namespace' => 'Admin\Orders'], function () {
	Route::get('/', 'OrdersController@index')->name('admin.orders');
	Route::get('/show/{id}', 'OrdersController@show')->name('admin.orders.show');
	Route::get('/destroy/{id}', 'OrdersController@destroy')->name('admin.orders.destroy');
});


// admin car type section
Route::group(['prefix' => 'admin/carTypes', 'namespace' => 'Admin\Types'], function () {
	Route::get('/', 'TypesController@index')->name('admin.types');
	Route::get('/create', 'TypesController@create')->name('admin.types.create');
	Route::post('/', 'TypesController@store')->name('admin.types.store');
	Route::get('/destroy/{id}', 'TypesController@destroy')->name('admin.types.destroy');
});


//  admin all vehicles section 

Route::group(['prefix' => 'admin/vehicles', 'namespace' => 'Admin\Vehicle'], function () {
	Route::get('/', 'VehiclesController@index')->name('admin.vehicle');
	Route::get('/create', 'VehiclesController@create')->name('admin.vehicle.create');
	Route::post('/', 'VehiclesController@store')->name('admin.vehicle.store');
	Route::get('/destroy/{id}', 'VehiclesController@destroy')->name('admin.vehicle.destroy');
});


// user order pick
Route::group(['prefix' => '', 'namespace' => 'backend'], function () {
	// Route::get('/', 'OrderController@index')->name('order');
	// Route::get('/create', 'OrderController@create')->name('order.create');
	Route::post('order/{type_id}', 'OrderController@store')->name('order.store');
	Route::get('/destroy/{id}', 'OrderController@destroy')->name('order.destroy');

});

Route::post('mail', "MailController@mail")->name('order.mail');