<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Car Rent&nbsp; | &nbsp;ADMIN</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">
    <link href="{{ asset('admin/css/jquery-ui.css') }}" type="text/css" rel="stylesheet" /> 
    
    {{-- input css --}}
    <link rel="stylesheet" href="{{ asset('files/css/assets/input.css')}}"> 
    
    {{-- nice-select css --}}
    <link rel="stylesheet" href="{{ asset('files/css/nice-select.css')}}"> 
    
    {{-- global style css --}}
    <link rel="stylesheet" href="{{ asset('files/css/global-style.css')}}"> 
    
    {{-- toastr css --}}
    <link rel="stylesheet" href="{{ asset('admin/css/toastr.min.css')}}"> 
    
    {{-- date-picker css --}}
    <link rel="stylesheet" href="{{ asset('files/css/datepicker.min.css')}}"> 
    
    {{-- files custom css --}}
    <link rel="stylesheet" href="{{ asset('files/css/style.css')}}"> 
    
    {{-- admin custom css --}}
    <link rel="stylesheet" href="{{ asset('admin/css/main.css')}}">
    <link rel="stylesheet" href="{{ asset('admin/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('admin/css/responsive.css')}}">
</head>

<body class="app">
    <div id="app">
        <main class="app-content" style="margin-left:0px; margin-top:0px; padding-top:100px;">

            @yield('content')
        </main>
    </div>

    {{--  <!-- Scripts -->  --}}
    <script src="{{ asset('admin/js/jquery-3.3.1.min.js') }}" crossorigin="anonymous"></script>
    <script src="{{ asset('admin/js/popper.min.js') }}" defer></script>
    {{--
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script> --}}
    <script src="{{ asset('admin/js/jquery-ui.js') }}"></script>
    <script src="{{ asset('admin/js/bootstrap.min.js') }}" defer></script>

    {{-- nice-select js --}}
    <script src="{{ asset('files/js/jquery.nice-select.min.js')}}"></script>

    {{-- toastr js --}}
    <script src="{{ asset('admin/js/toastr.min.js')}}"></script>

    {{-- fire-ui js --}}
    <script src="{{ asset('files/js/main.js')}}"></script>

    {{-- date-picker js --}}
    <script src="{{ asset('files/js/datepicker.min.js')}}"></script>
    <script src="{{ asset('files/js/datepicker.en.js')}}"></script>

    {{--
    <!-- =========New JS Files======== -->--}}
    <script src="{{ asset('admin/js/main.js')}}"></script>
    <script src="{{ asset('admin/js/script.js')}}"></script>
    <script>
        @if(Session::has('success'))
        toastr.success("{{Session::get('success')}}");
        @endif

        @if(Session::has('error'))
        toastr.error("{{Session::get('error')}}");
        @endif
    </script>
</body>

</html>