<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Car Rent&nbsp; | &nbsp;ADMIN</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">
    <link href="{{ asset('admin/css/jquery-ui.css') }}" type="text/css" rel="stylesheet" /> 
    
    {{-- input css --}}
    <link rel="stylesheet" href="{{ asset('files/css/assets/input.css')}}"> 
    
    {{-- nice-select css --}}
    <link rel="stylesheet" href="{{ asset('files/css/nice-select.css')}}"> 
    
    {{-- global style css --}}
    <link rel="stylesheet" href="{{ asset('files/css/global-style.css')}}"> 
    
    {{-- toastr css --}}
    <link rel="stylesheet" href="{{ asset('admin/css/toastr.min.css')}}"> 
    
    {{-- date-picker css --}}
    <link rel="stylesheet" href="{{ asset('files/css/datepicker.min.css')}}"> 
    
    {{-- files custom css --}}
    <link rel="stylesheet" href="{{ asset('files/css/style.css')}}"> 
    
    @yield('stylesheet')

    {{-- admin custom css --}}
    <link rel="stylesheet" href="{{ asset('admin/css/main.css')}}">
    <link rel="stylesheet" href="{{ asset('admin/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('admin/css/responsive.css')}}">

    <link rel="shortcut icon" href="{{ asset('files/images/car.png') }}" type="image/x-icon">

    <style>
    .dropdown-item {
        color: #ffffff;
        background-color: #191e28;
        padding: 14px 20px;
        font-weight: 700;
        text-transform: uppercase;
        letter-spacing: 2px;
    }
    </style>
</head>

<body class="app sidebar-mini rtl">
    <div id="app">
        <header class="app-header">
            <a class="app-header__logo" href="{{ url('/home') }}">
                {{-- <img src="{{ asset('files/images/logo.png') }}" alt="Logo" style="max-width: 140px;"> --}}
                T-CAR
            </a>
            <!-- Sidebar toggle button-->
            <a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"><i class="fas fa-bars"></i></a>
            <!-- Navbar Right Menu-->
            <ul class="app-nav">
                <!-- User Menu-->
                <li>
                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i>Logout</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </header>
        
        {{--  <!-- Sidebar menu-->  --}}
        <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
        <aside class="app-sidebar">
            <div class="app-sidebar__user">
                <img class="app-sidebar__user-avatar" src="{{ asset('admin/images/profile.png') }}" alt="User Image">
                <div>
                    <p class="app-sidebar__user-name">Tajkia's Car Booking</p>
                    <p class="app-sidebar__user-designation">Admin</p>
                </div>
            </div>
            <ul class="app-menu">
                {{-- Dashboard --}}
                <li>
                    <a class="app-menu__item" href="{{ url('/home') }}">
                        <i class="app-menu__icon fas fa-dove"></i>
                        <span class="app-menu__label">Dashboard</span>
                    </a>
                </li>

                {{-- Booking --}}
                <li>
                    <a class="app-menu__item" href="{{ route('admin.orders') }}">
                        <i class="app-menu__icon far fa-bell"></i>
                        <span class="app-menu__label">Orders</span>
                    </a>
                </li>

                {{-- Services --}}
                <li class="treeview">
                    <a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fas fa-taxi"></i><span class="app-menu__label">Services</span><i class="treeview-indicator fa fa-angle-right"></i></a>

                    <ul class="treeview-menu">

                        <li><a class="treeview-item" href="{{ route('admin.vehicle') }}"><i class="icon"></i> Vehicles</a></li>
                    </ul>
                </li>
                {{-- Services --}}
                <li class="treeview">
                    <a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fas fa-cog"></i><span class="app-menu__label">Settings</span><i class="treeview-indicator fa fa-angle-right"></i></a>

                    <ul class="treeview-menu">

                        <li><a class="treeview-item" href="{{ route('admin.types') }}"><i class="icon"></i> Types</a></li>
                    </ul>
                </li>
            </ul>
        </aside>
        <main class="app-content">

            @yield('content')
        </main>
    </div>

    {{--  <!-- Scripts -->  --}}
    <script src="{{ asset('admin/js/jquery-3.3.1.min.js') }}" crossorigin="anonymous"></script>
    <script src="{{ asset('admin/js/popper.min.js') }}" defer></script>
    {{--
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script> --}}
    <script src="{{ asset('admin/js/jquery-ui.js') }}"></script>
    <script src="{{ asset('admin/js/bootstrap.min.js') }}" defer></script>

    {{-- nice-select js --}}
    <script src="{{ asset('files/js/jquery.nice-select.min.js')}}"></script>

    {{-- toastr js --}}
    <script src="{{ asset('admin/js/toastr.min.js')}}"></script>

    {{-- fire-ui js --}}
    <script src="{{ asset('files/js/main.js')}}"></script>

    {{-- date-picker js --}}
    <script src="{{ asset('files/js/datepicker.min.js')}}"></script>
    <script src="{{ asset('files/js/datepicker.en.js')}}"></script>

    {{--
    <!-- =========New JS Files======== -->--}}
    <script src="{{ asset('admin/js/main.js')}}"></script>
    <script src="{{ asset('admin/js/script.js')}}"></script>
    <script>
        @if(Session::has('success'))
        toastr.success("{{Session::get('success')}}");
        @endif

        @if(Session::has('error'))
        toastr.error("{{Session::get('error')}}");
        @endif
    </script>
</body>

</html>