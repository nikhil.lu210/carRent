@extends('includes.main')

@section('content')
@foreach($errors->all() as $error)
    <ul>
        <li>{{ $error }}</li>
    </ul>
@endforeach
{{-- ============< Carousel Part Starts >=========== --}}
<section class="carousel-part">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="cp-left welcome-msg text-center animated zoomIn">
                            <h3 class="color_ff">WELCOME TO</h3>
                            <h1 style="color: #ffbe00;">Best Rent A Car.<span style="font-size: 16px">com</span></h1>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <form class="fire_input2" method="post" action="{{ route('booking.store')}}">
                        @csrf
                        <div class="cp-right search-form animated zoomIn">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="fire_input1">
                                        <input type="text" placeholder="Name" name="clientName" class="input1">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="fire_input1">
                                        <input type="text" placeholder="Contact Number" name="mobileNumber" class="input1">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="fire_input1">
                                        <input type="text" placeholder="E-mail Address" name="emailAddress" class="input1">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="fire_input1">
                                        <input type="text" placeholder="Pick Up Point" name="pickPoint" class="input1">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="fire_input1">
                                        <input type="text" placeholder="Drop Off Point" name="dropPoint" class="input1">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="fire_input1">
                                        <input type="text" placeholder="Start Date" name="startDate" class="input1 datepicker-here" data-position="bottom left" data-language='en' >
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="fire_input1">
                                        <input type="text" placeholder="End Date" name="endDate" class="input1 datepicker-here" data-position="bottom left" data-language='en' >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="fire_select2 fire_select">
                                        <select name="vahicleType" id="vehicleType">
                                            <option value="">Type</option>
                                            <option value="car" id="car">Car</option>
                                            <option value="hiace" id="hiace">Hiace</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="fire_select2 fire_select">
                                        <select name="vahicleModel" id="vehicleModel">
                                            <option value="">Model</option>
                                            <option value="Model-01" class="car" id="car1">Model-01</option>
                                            <option value="Model-02" class="hiace" id="hiace1">Model-02</option>
                                            {{--  <option value="Model-03" class="car">Model-03</option>
                                            <option value="Model-03" class="hiace">Model-04</option>
                                            <option value="Model-03" class="car">Model-05</option>
                                            <option value="Model-03" class="hiace">Model-06</option>
                                            <option value="Model-03" class="car">Model-07</option>  --}}
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="fire_input1">
                                        <input type="text" placeholder="Quantity" name="vahicleQuantity" class="input1">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <button class="btn btn-info custom-btn btn-block btn-lg" type="submit">Book Now</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- =============< Carousel Part Ends >============ --}}


{{-- ============< About Part Starts >=========== --}}
<section class="about-part" id="about">
    <div class="container">
        <div class="row">
            <div class="col-md-12 m-t-70 m-b-70">
                <div class="about-us" data-aos="zoom-in" data-aos-delay="100" data-aos-duration="1000">
                    <div class="about heading text-center">
                        <h1 class="color_ff">know <span style="color:#ffbe00; font-weight:700; text-transform:uppercase;">ABOUT US</span></h1>
                        <p class="m-t-20">
                            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Libero exercitationem sint inventore cum dolorum totam, doloremque odit ipsum quas placeat. Lorem ipsum dolor sit amet consectetur adipisicing elit. Mollitia non voluptas error accusamus, sapiente eveniet! Porro laboriosam quia hic fugiat totam nisi consectetur ut quibusdam perferendis mollitia. Eius harum rerum eveniet omnis totam quaerat. Adipisci perferendis deleniti aliquam exercitationem non cum eaque voluptatibus nostrum nam, molestias laudantium pariatur, iste impedit.
                        </p>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="about-us m-t-20">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="our-vision">
                                            <h3 class="color_ff">our <span style="color:#ffbe00; font-weight:700; text-transform:uppercase;">Vision</span></h3>
                                            <hr>
                                            <p>
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto praesentium similique cumque ullam molestias sequi optio ad commodi nemo eaque id ratione ipsum voluptas itaque consequatur iure eos numquam perferendis tempora quos, assumenda, totam sint quae! <br> Placeat quibusdam harum ipsam. Quia animi aliquam minima dignissimos aperiam voluptate et quas hic iure sed temporibus debitis sunt modi molestiae nihil fuga excepturi, nam laudantium illo, aspernatur adipisci nisi.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="our-goal">
                                            <h3 class="color_ff">our <span style="color:#ffbe00; font-weight:700; text-transform:uppercase;">Goal</span></h3>
                                            <hr>
                                            <ul>
                                                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                                                <li>Nullam elementum nisi eget mi mollis laoreet.</li>
                                                <li>Morbi non dignissim tellus, vitae blandit urna Lorem ipsum dolor sit amet, consectetur adipiscing elit morbi non dignissim.</li>
                                                <li>Nullam elementum nisi eget mi mollis laoreet.</li>
                                                <li>Morbi non dignissim tellus, vitae blandit urna Lorem ipsum dolor sit amet.</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- =============< About Part Ends >============ --}}


{{-- ============< Car-Service Part Starts >=========== --}}
<section class="car-service-part pricing_section style2 col4 p-t-50 p-b-50">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="car-service" data-aos="zoom-in" data-aos-delay="100" data-aos-duration="1000">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading text-center p-b-50">
                                <h1 class="color_ff">we pay rent <a href="#" style="color:#ffbe00; font-weight:700; text-transform:uppercase;">cars</a></h1>
                            </div>
                        </div>
                    </div>
                    <div class="pricing_wrapper clearfix">
                        <div class="single_table text-center radius_10">
                            <div class="tbl_header">
                                <img src="{{ asset('files/images/Toyota_Camry.png')}}" class="img-thumbnail rounded mx-auto d-block" alt="...">
                                <p class="fw_500">
                                    <a href="#" class="deal-title">Toyota Camry</a>
                                </p>
                            </div>
                            <div class="tbl_body">
                                <ul class="list">
                                    <li>Door: <span class="float-right color_ff fw_500">4</span></li>
                                    <li>Passengers: <span class="float-right color_ff fw_500">4</span></li>
                                    <li>Luggage: <span class="float-right color_ff fw_500">2</span></li>
                                    <li>Air-Condition: <span class="float-right color_ff fw_500">Yes</span></li>
                                </ul>
                                <a class="purchase" href="#" data-toggle="modal" data-target="#bookCar">Book Now</a>
                            </div>
                        </div>
                        <div class="single_table text-center radius_10">
                            <div class="tbl_header">
                                <img src="{{ asset('files/images/Toyota_Fortuner.png')}}" class="img-thumbnail rounded mx-auto d-block" alt="...">
                                <p class="fw_500">
                                    <a href="#" class="deal-title">Toyota Fortuner</a>
                                </p>
                            </div>
                            <div class="tbl_body">
                                <ul class="list">
                                    <li>Door: <span class="float-right color_ff fw_500">4</span></li>
                                    <li>Passengers: <span class="float-right color_ff fw_500">4</span></li>
                                    <li>Luggage: <span class="float-right color_ff fw_500">2</span></li>
                                    <li>Air-Condition: <span class="float-right color_ff fw_500">Yes</span></li>
                                </ul>
                                <a class="purchase" href="#" data-toggle="modal" data-target="#bookCar">Book Now</a>
                            </div>
                        </div>
                        <div class="single_table text-center radius_10">
                            <div class="tbl_header">
                                <img src="{{ asset('files/images/Toyota_Innova_Crysta.png')}}" class="img-thumbnail rounded mx-auto d-block" alt="...">
                                <p class="fw_500">
                                    <a href="#" class="deal-title">Toyota Innova Crysta</a>
                                </p>
                            </div>
                            <div class="tbl_body">
                                <ul class="list">
                                    <li>Door: <span class="float-right color_ff fw_500">4</span></li>
                                    <li>Passengers: <span class="float-right color_ff fw_500">4</span></li>
                                    <li>Luggage: <span class="float-right color_ff fw_500">2</span></li>
                                    <li>Air-Condition: <span class="float-right color_ff fw_500">Yes</span></li>
                                </ul>
                                <a class="purchase" href="#" data-toggle="modal" data-target="#bookCar">Book Now</a>
                            </div>
                        </div>
                        <div class="single_table text-center radius_10">
                            <div class="tbl_header">
                                <img src="{{ asset('files/images/Mahindra_XUV500.png')}}" class="img-thumbnail rounded mx-auto d-block" alt="...">
                                <p class="fw_500">
                                    <a href="#" class="deal-title">Mahindra XUV500</a>
                                </p>
                            </div>
                            <div class="tbl_body">
                                <ul class="list">
                                    <li>Door: <span class="float-right color_ff fw_500">4</span></li>
                                    <li>Passengers: <span class="float-right color_ff fw_500">4</span></li>
                                    <li>Luggage: <span class="float-right color_ff fw_500">2</span></li>
                                    <li>Air-Condition: <span class="float-right color_ff fw_500">Yes</span></li>
                                </ul>
                                <a class="purchase" href="#" data-toggle="modal" data-target="#bookCar">Book Now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- =============< Car-Service Part Ends >============ --}}

{{--  =======> Car Book Now Modal <=======  --}}
<div class="modal fade" id="bookCar" tabindex="-1" role="dialog" aria-labelledby="bookCarLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="bookCarLabel">Confirm Your Booking</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" style="color:#efefef;">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="cp-right search-form">
                <div class="row">
                    <div class="col-md-12">
                        <div class="fire_input1">
                            <input type="text" placeholder="Name" name="name" class="input1">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="fire_input1">
                            <input type="text" placeholder="Contact Number" name="contactNo" class="input1">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="fire_input1">
                            <input type="text" placeholder="E-mail Address" name="mailAddress" class="input1">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="fire_input1">
                            <input type="text" placeholder="Pick Up Point" name="pickUpPoint" class="input1">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="fire_input1">
                            <input type="text" placeholder="Drop Off Point" name="dropOffPoint" class="input1">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="fire_input1">
                            <input type="text" placeholder="Start Date & Time" name="startDayTime" class="input1 datepicker-here" data-position="bottom left" data-language='en' >
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="fire_input1">
                            <input type="text" placeholder="End Date & Time" name="endDayTime" class="input1 datepicker-here" data-position="bottom left" data-language='en' >
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="fire_select2 fire_select">
                            <select name="" id="">
                                <option value="">Type</option>
                                <option value="Car">Car</option>
                                <option value="Hiace">Hiace</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="fire_select2 fire_select">
                            <select name="" id="">
                                <option value="">Model</option>
                                <option value="Model-01">Model-01</option>
                                <option value="Model-02">Model-02</option>
                                <option value="Model-03">Model-03</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="fire_input1">
                            <input type="text" placeholder="Quantity" name="vahicleQuantity" class="input1">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-info custom-btn" style="color: #efefef;
            background-color: #2f3542;
            border: 1px solid #2f3542;
            border-color: #2f3542;" data-dismiss="modal">Cancle</button>
            <button type="submit" class="btn btn-info custom-btn">Confirm</button>
        </div>
        </div>
    </div>
</div>
{{--  =======> Car Book Now Modal <=======  --}}

{{-- ============< Hiace-Service Part Starts >=========== --}}
<section class="hiace-service-part pricing_section style2 col4 p-t-50 p-b-50">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="car-service" data-aos="zoom-in" data-aos-delay="100" data-aos-duration="1000">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading text-center p-b-50">
                                <h1 class="color_ff">we pay rent <a href="#" style="color:#ffbe00; font-weight:700; text-transform:uppercase;">Hiace</a></h1>
                            </div>
                        </div>
                    </div>
                    <div class="pricing_wrapper clearfix">
                        <div class="single_table text-center radius_10">
                            <div class="tbl_header">
                                <img src="{{ asset('files/images/hiace.png')}}" class="img-thumbnail rounded mx-auto d-block" alt="...">
                                <p class="fw_500">
                                    <a href="#" class="deal-title">CBA-TRH224W-LDPNK</a>
                                </p>
                            </div>
                            <div class="tbl_body">
                                <ul class="list">
                                    <li>Door: <span class="float-right color_ff fw_500">4</span></li>
                                    <li>Passengers: <span class="float-right color_ff fw_500">4</span></li>
                                    <li>Luggage: <span class="float-right color_ff fw_500">2</span></li>
                                    <li>Air-Condition: <span class="float-right color_ff fw_500">Yes</span></li>
                                </ul>
                                <a class="purchase" href="#" data-toggle="modal" data-target="#bookHiace">Book Now</a>
                            </div>
                        </div>
                        <div class="single_table text-center radius_10">
                            <div class="tbl_header">
                                <img src="{{ asset('files/images/hiace.png')}}" class="img-thumbnail rounded mx-auto d-block" alt="...">
                                <p class="fw_500">
                                    <a href="#" class="deal-title">CBA-TRH214W-JDPDK</a>
                                </p>
                            </div>
                            <div class="tbl_body">
                                <ul class="list">
                                    <li>Door: <span class="float-right color_ff fw_500">4</span></li>
                                    <li>Passengers: <span class="float-right color_ff fw_500">4</span></li>
                                    <li>Luggage: <span class="float-right color_ff fw_500">2</span></li>
                                    <li>Air-Condition: <span class="float-right color_ff fw_500">Yes</span></li>
                                </ul>
                                <a class="purchase" href="#" data-toggle="modal" data-target="#bookHiace">Book Now</a>
                            </div>
                        </div>
                        <div class="single_table text-center radius_10">
                            <div class="tbl_header">
                                <img src="{{ asset('files/images/hiace.png')}}" class="img-thumbnail rounded mx-auto d-block" alt="...">
                                <p class="fw_500">
                                    <a href="#" class="deal-title">CBA-TRH229W-LDPNK</a>
                                </p>
                            </div>
                            <div class="tbl_body">
                                <ul class="list">
                                    <li>Door: <span class="float-right color_ff fw_500">4</span></li>
                                    <li>Passengers: <span class="float-right color_ff fw_500">4</span></li>
                                    <li>Luggage: <span class="float-right color_ff fw_500">2</span></li>
                                    <li>Air-Condition: <span class="float-right color_ff fw_500">Yes</span></li>
                                </ul>
                                <a class="purchase" href="#" data-toggle="modal" data-target="#bookHiace">Book Now</a>
                            </div>
                        </div>
                        <div class="single_table text-center radius_10">
                            <div class="tbl_header">
                                <img src="{{ asset('files/images/hiace.png')}}" class="img-thumbnail rounded mx-auto d-block" alt="...">
                                <p class="fw_500">
                                    <a href="#" class="deal-title">CBA-KDH227B-LDPNK</a>
                                </p>
                            </div>
                            <div class="tbl_body">
                                <ul class="list">
                                    <li>Door: <span class="float-right color_ff fw_500">4</span></li>
                                    <li>Passengers: <span class="float-right color_ff fw_500">4</span></li>
                                    <li>Luggage: <span class="float-right color_ff fw_500">2</span></li>
                                    <li>Air-Condition: <span class="float-right color_ff fw_500">Yes</span></li>
                                </ul>
                                <a class="purchase" href="#" data-toggle="modal" data-target="#bookHiace">Book Now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- =============< Hiace-Service Part Ends >============ --}}


{{--  =======> Hiace Book Now Modal <=======  --}}
<div class="modal fade" id="bookHiace" tabindex="-1" role="dialog" aria-labelledby="bookHiaceLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="bookHiaceLabel">Confirm Your Booking</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" style="color:#efefef;">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="cp-right search-form">
                <div class="row">
                    <div class="col-md-12">
                        <div class="fire_input1">
                            <input type="text" placeholder="Name" name="name" class="input1">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="fire_input1">
                            <input type="text" placeholder="Contact Number" name="contactNo" class="input1">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="fire_input1">
                            <input type="text" placeholder="E-mail Address" name="mailAddress" class="input1">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="fire_input1">
                            <input type="text" placeholder="Pick Up Point" name="pickUpPoint" class="input1">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="fire_input1">
                            <input type="text" placeholder="Drop Off Point" name="dropOffPoint" class="input1">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="fire_input1">
                            <input type="text" placeholder="Start Date & Time" name="startDayTime" class="input1 datepicker-here" data-position="bottom left" data-language='en' >
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="fire_input1">
                            <input type="text" placeholder="End Date & Time" name="endDayTime" class="input1 datepicker-here" data-position="bottom left" data-language='en' >
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="fire_select2 fire_select">
                            <select name="" id="">
                                <option value="">Type</option>
                                <option value="Car">Car</option>
                                <option value="Hiace">Hiace</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="fire_select2 fire_select">
                            <select name="" id="">
                                <option value="">Model</option>
                                <option value="Model-01">Model-01</option>
                                <option value="Model-02">Model-02</option>
                                <option value="Model-03">Model-03</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="fire_input1">
                            <input type="text" placeholder="Quantity" name="vahicleQuantity" class="input1">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-info custom-btn" style="color: #efefef;
            background-color: #2f3542;
            border: 1px solid #2f3542;
            border-color: #2f3542;" data-dismiss="modal">Cancle</button>
            <button type="submit" class="btn btn-info custom-btn">Confirm</button>
        </div>
        </div>
    </div>
</div>
{{--  =======> Hiace Book Now Modal <=======  --}}


{{-- ============< Contact Part Starts >=========== --}}
<section class="contact-part" id="contact">
    <div class="container">
        <div class="row">
            <div class="col-md-12 m-t-70 m-b-70">
                <div class="contact-us" data-aos="zoom-in" data-aos-delay="100" data-aos-duration="1000" data-aos-easing="ease-in-out">
                    <div class="row">
                        <div class="col-md-4 address">
                            <div class="row">
                                <div class="col-md-12 m-b-25">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <i class="fas fa-home fa-2x"></i>
                                        </div>
                                        <div class="col-md-9">
                                            <h5 class="contact-header">contact address</h5>
                                            <p>Best Rent a Car, Zindabazar, Sylhet, Bangladesh</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 m-b-25">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <i class="fas fa-phone fa-2x"></i>
                                        </div>
                                        <div class="col-md-9">
                                            <h5 class="contact-header">contact number</h5>
                                            <p>+880 1711111111 <br />+880 1722222222</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <i class="fas fa-envelope fa-2x"></i>
                                        </div>
                                        <div class="col-md-9">
                                            <h5 class="contact-header">mail address</h5>
                                            <p>email@gmail.com <br />email@gmail.com</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-12"><h5 class="contact-header">contact form</h5></div>
                                <div class="col-md-12">
                                    <div class="fire_input1">
                                        <input type="text" placeholder="Name" name="name" class="input1">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="fire_input1">
                                        <input type="text" placeholder="Email" name="email" class="input1">
                                    </div>
                                </div>
                                <div class="col-md-12 textarea ">
                                    <div class="fire_input1">
                                        <textarea rows="3" placeholder="Message" name="message" class="input1"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn btn-info custom-btn btn-lg float-right">Send Message</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- =============< Contact Part Ends >============ --}}





{{-- ============< Part Starts >=========== --}}

{{-- =============< Part Ends >============ --}}


{{-- ============< Part Starts >=========== --}}

{{-- =============< Part Ends >============ --}}


{{-- ============< Part Starts >=========== --}}

{{-- =============< Part Ends >============ --}}


@endsection