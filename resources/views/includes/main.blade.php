<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{--  CSRF Token  --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Car Rent</title>

    {{-- google font --}}
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">

    {{-- font awesome css --}}
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    
    {{--  bootstrap css  --}}
    <link rel="stylesheet" href="{{ asset('files/css/bootstrap.min.css')}}">

    {{-- navbar css --}}
    <link rel="stylesheet" href="{{ asset('files/css/assets/navbar.css')}}">

    {{-- input css --}}
    <link rel="stylesheet" href="{{ asset('files/css/assets/input.css')}}">

    {{-- nice-select css --}}
    <link rel="stylesheet" href="{{ asset('files/css/nice-select.css')}}">

    {{-- pricing table css --}}
    <link rel="stylesheet" href="{{ asset('files/css/assets/pricing-tbl.css')}}">

    {{-- Carousel CSS --}}
    <link rel="stylesheet" href="{{ asset('files/css/owl/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('files/css/assets/carousel.css') }}">
    
    {{--  toastr css  --}}
    <link rel="stylesheet" href="{{ asset('admin/css/toastr.min.css')}}">

    {{--  global style css  --}}
    <link rel="stylesheet" href="{{ asset('files/css/global-style.css')}}">

    {{--  date-picker css  --}}
    <link rel="stylesheet" href="{{ asset('files/css/datepicker.min.css')}}">

    {{--  animate css  --}}
    <link rel="stylesheet" href="{{ asset('files/css/animate/animate.min.css')}}">

    {{-- aos css --}}
    <link rel="stylesheet" href="{{ asset('files/css/aos/aos.css')}}">

    {{-- custom css --}}
    <link rel="stylesheet" href="{{ asset('files/css/margin-padding.css')}}">
    <link rel="stylesheet" href="{{ asset('files/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('files/css/responsive.css')}}">

</head>
<body data-spy="scroll" data-target="#navbar" data-offset="50">
    <div id="app">
        <header class="navbar-part bg_f8f fire_header2 header_common fixed-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <nav class="navbar navbar-expand-lg navbar-light" id="navbar">
                            <a class="navbar-brand order-1 order-lg-1" href="#">
                                T-CAR
                            </a>
                            <button class="navbar-toggler order-3" type="button" data-toggle="collapse" data-target="#navbar2" aria-controls="navbar1" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
    
                            <div class="collapse navbar-collapse order-4 order-lg-2" id="navbar2">
                                <ul class="navbar-nav ml-auto">
                                    <li><a class="nav-menu" href="/">Home</a></li>
                                    <li><a class="nav-menu" href="#about">About Us</a></li>
                                    <li><a class="nav-menu" href="#service">Services</a></li>
                                    <li><a class="nav-menu" href="#contact">Contact</a></li>
                                </ul>
                            </div>
                            <ul class="social_contact d_inline pos_relative order-2 order-lg-3">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                <li><a href="#"><i class="fab fa-dribbble"></i></a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </header>
    
        <main class="app-content">
            
            @yield('content')
        </main>
    </div>


{{-- ============< Footer Part Starts >=========== --}}
<section class="footer p-t-30 p-b-25">
    <div class="container" data-aos="zoom-in" data-aos-delay="100" data-aos-duration="2000">
        <div class="row">					
            <div class="col-md-4 offset-md-4 text-center">
                <div class="row">
                    <div class="col-md-12 m-b-25 social-media">
                        <ul>
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fab fa-google-plus-g"></i></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-12 copyright text-center">
                © Copyright 2019 <span>RENTaCAR</span> &nbsp; | &nbsp; All Rights Reseved | &nbsp; Developed By <a href="https://www.facebook.com/profile.php?id=100009337230924" target="_blank">TAJKIA MAHMUB</a>  
            </div>
        </div>
    </div>
</section>

{{-- =============< Footer Part Ends >============ --}}


{{-- ==============< Scroll-Top-Part Starts >============= --}}
    <div id="stop" class="scrollTop">
        <span><a href=""><i class="fas fa-chevron-up"></i></a></span>
    </div>
{{-- ===============< Scroll-Top-Part Ends >============== --}}

    {{-- jQuary library --}}
    <script src="{{ asset('files/js/jquery.min.js')}}"></script>

    {{-- bootstrap js --}}
    <script src="{{ asset('files/js/popper.min.js')}}"></script>
    <script src="{{ asset('files/js/bootstrap.min.js')}}"></script>

    {{-- nice-select js --}}
    <script src="{{ asset('files/js/jquery.nice-select.min.js')}}"></script>

    {{-- Owl Carousel --}}
    <script src="{{ asset('files/js/owl/owl.carousel.min.js') }}"></script>

    {{-- date-picker js --}}
    <script src="{{ asset('files/js/datepicker.min.js')}}"></script>
    <script src="{{ asset('files/js/datepicker.en.js')}}"></script>

    {{--  toastr js  --}}
    <script src="{{ asset('admin/js/toastr.min.js')}}"></script>

    {{-- aos js --}}
    <script src="{{ asset('files/js/aos/aos.js')}}"></script>
    <script>
        AOS.init();
    </script>

    {{-- all jQuary activation code here and always it will be bottom of all script tag --}}
    <script src="{{ asset('files/js/main.js')}}"></script>

    {{-- custom js --}}
    <script src="{{ asset('files/js/script.js')}}"></script>
    <script>
        @if(Session::has('success'))
            toastr.success("{{Session::get('success')}}");
        @endif

        @if(Session::has('error'))
            toastr.error("{{Session::get('error')}}");
        @endif
    </script>
</body>
</html>
