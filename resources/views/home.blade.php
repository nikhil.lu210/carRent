@extends('layouts.app')

@section('content')
<section class="dashboard-part">
    <div class="container">
        <div class="row">
            @foreach ($types as $type)
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <h4>{{ $type->name }}</h4>
                        </div>
                        <div class="card-body">
                            <h2 class="color_ff">Total {{ $type->name }}: 
                                <span class="float-right" style="color:#ffbe00;">
                                    {{ App\Vahical::where('type_id', $type->id)->count() }}
                                </span>
                            </h2>
                        </div>
                    </div>
                </div>
            @endforeach
            
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h4>Total Booking</h4>
                    </div>
                    <div class="card-body">
                        <h2 class="color_ff">Total Booking: 
                            <span class="float-right" style="color:#ffbe00;">
                                {{ App\Order::all()->count() }}
                            </span>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
