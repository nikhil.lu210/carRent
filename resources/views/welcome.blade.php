@extends('includes.main')

@section('content')
{{--  @foreach($errors->all() as $error)
    <ul>
        <li>{{ $error }}</li>
    </ul>
@endforeach  --}}
{{-- ============< Carousel Part Starts >=========== --}}
<section class="carousel-part" id="home">
    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-md-3 text-center">
                <div class="cp-left welcome-msg text-center animated zoomIn">
                    <h3 class="color_ff">WELCOME TO</h3>
                    <h1 style="color: #ffbe00;">Tajkia Car Booking</h1>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- =============< Carousel Part Ends >============ --}}



{{-- ============< About Part Starts >=========== --}}
<section class="about-part" id="about">
    <div class="container">
        <div class="row">
            <div class="col-md-12 m-t-70 m-b-70">
                <div class="about-us" data-aos="zoom-in" data-aos-delay="100" data-aos-duration="1000">
                    <div class="about heading text-center">
                        <h1 class="color_ff">know <span style="color:#ffbe00; font-weight:700; text-transform:uppercase;">ABOUT US</span></h1>
                        <p class="m-t-20">
                            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Libero exercitationem sint inventore cum dolorum totam, doloremque odit ipsum quas placeat. Lorem ipsum dolor sit amet consectetur adipisicing elit. Mollitia non voluptas error accusamus, sapiente eveniet! Porro laboriosam quia hic fugiat totam nisi consectetur ut quibusdam perferendis mollitia. Eius harum rerum eveniet omnis totam quaerat. Adipisci perferendis deleniti aliquam exercitationem non cum eaque voluptatibus nostrum nam, molestias laudantium pariatur, iste impedit.
                        </p>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="about-us m-t-20">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="our-vision">
                                            <h3 class="color_ff">our <span style="color:#ffbe00; font-weight:700; text-transform:uppercase;">Vision</span></h3>
                                            <hr>
                                            <p>
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto praesentium similique cumque ullam molestias sequi optio ad commodi nemo eaque id ratione ipsum voluptas itaque consequatur iure eos numquam perferendis tempora quos, assumenda, totam sint quae! <br> Placeat quibusdam harum ipsam. Quia animi aliquam minima dignissimos aperiam voluptate et quas hic iure sed temporibus debitis sunt modi molestiae nihil fuga excepturi, nam laudantium illo, aspernatur adipisci nisi.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="our-goal">
                                            <h3 class="color_ff">our <span style="color:#ffbe00; font-weight:700; text-transform:uppercase;">Goal</span></h3>
                                            <hr>
                                            <ul>
                                                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                                                <li>Nullam elementum nisi eget mi mollis laoreet.</li>
                                                <li>Morbi non dignissim tellus, vitae blandit urna Lorem ipsum dolor sit amet, consectetur adipiscing elit morbi non dignissim.</li>
                                                <li>Nullam elementum nisi eget mi mollis laoreet.</li>
                                                <li>Morbi non dignissim tellus, vitae blandit urna Lorem ipsum dolor sit amet.</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- =============< About Part Ends >============ --}}

@foreach ($types as $type)

<?php
    $items = App\Vahical::where('type_id', $type->id)
       ->orderBy('id', 'desc')
       ->take(4)
       ->get();
    if(isset($items[0]) == false) continue; 
 ?>

{{-- ============< Car-Service Part Starts >=========== --}}
<section class="car-service-part pricing_section style2 col4 p-t-50 p-b-50" id="service">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="car-service" data-aos="zoom-in" data-aos-delay="100" data-aos-duration="1000">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading text-center p-b-50">
                                <h1 class="color_ff">we pay rent <a href="#" style="color:#ffbe00; font-weight:700; text-transform:uppercase;">{{ $type->name }}S</a></h1>
                            </div>
                        </div>
                    </div>
                    <div class="pricing_wrapper clearfix">
                        @foreach($items as $item)
                        <div class="single_table text-center radius_10">
                            <div class="tbl_header">
                                <img src="{{ asset('vehicle_image/'.$item->avatar)}}" class="img-thumbnail rounded mx-auto d-block" alt="...">
                                <p class="fw_500">
                                    <a href="#" class="deal-title">{{ $item->name }}</a>
                                </p>
                            </div>
                            <div class="tbl_body">
                                <ul class="list">
                                    <li>Seat: <span class="float-right color_ff fw_500">{{ $item->seat_no }}</span></li>
                                    <li>{{ $type->name }} Reg: no: <span class="float-right color_ff fw_500">{{ $item->reg_no }}</span></li>
                                    <li>Cost/Day(1 way) <span class="float-right color_ff fw_500">৳ {{ $item->perdat_cost_1_way }} </span></li>
                                    <li>Cost/Day(2 way) <span class="float-right color_ff fw_500">৳ {{ $item->perdat_cost_2_way }} </span></li>
                                </ul>
                                <a class="purchase" href="#" data-toggle="modal" data-target="#book{{ $type->name.$item->id }}">Book Now</a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- =============< Car-Service Part Ends >============ --}}

{{--  =======> Car Book Now Modal <=======  --}}
@foreach ($items as $item)
<div class="modal fade" id="book{{ $type->name.$item->id }}" tabindex="-1" role="dialog" aria-labelledby="book{{ $type->name }}Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title float-left" id="book{{ $type->name }}Label">Confirm Booking </h5>
                <h5 class="modal-title float-right"> ( Cost: <span class="total-cost">00</span><sup>tk</sup> )</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true" style="color:#efefef;">&times;</span>
                </button>
            </div>
            <form class="fire_input2" method="post" action="{{ route('order.store', ['type_id' => $type->id]) }}">
            @csrf
            <div class="modal-body">
                <div class="cp-right search-form">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="fire_input1">
                                <input type="text" placeholder="Name" name="user_name" class="input1{{ $errors->has('user_name') ? ' is-invalid' : '' }}" required>
                                @if ($errors->has('user_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('user_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="fire_input1">
                                <input type="text" placeholder="Contact Number" name="mobile" class="input1{{ $errors->has('mobile') ? ' is-invalid' : '' }}" required>
                                @if ($errors->has('mobile'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="fire_input1">
                            <input type="number" placeholder="Needed Vehicle" id="needed_vahicle{{$item->id}}" name="needed_vahicle" value="" class="input1{{ $errors->has('needed_vahicle') ? ' is-invalid' : '' }} needed-vehicle" required>
                                @if ($errors->has('needed_vahicle'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('needed_vahicle') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="fire_input1">
                                <input type="text" placeholder="Pick Up Point" name="pickPoint" class="input1{{ $errors->has('pickPoint') ? ' is-invalid' : '' }}" required>
                                @if ($errors->has('pickPoint'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('pickPoint') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="fire_input1">
                                <input type="text" placeholder="Drop Point" name="dropPoint" class="input1{{ $errors->has('dropPoint') ? ' is-invalid' : '' }}" required>
                                @if ($errors->has('dropPoint'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('dropPoint') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="fire_input1">
                                <input type="text" placeholder="Journey Date" name="journey_date" class="input1 datepicker-here{{ $errors->has('journey_date') ? ' is-invalid' : '' }}" data-position="bottom left" data-language='en' required >
                                @if ($errors->has('journey_date'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('journey_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="fire_select2 fire_select">
                            <select name="way" class="way" id="{{$item->id}}">
                                    <option >Select Way</option>
                                    <option value="1">One Way</option>
                            <option value="2">Two Way</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="group textarea">
                                <textarea placeholder="Extra Information Here" rows="2" name="note"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                        <input type="number" value="{{ $item->perdat_cost_1_way }}" id="way1cost{{$item->id}}" style="display:none;" name="way1cost">
                        <input type="number" value="{{ $item->perdat_cost_2_way }}" id="way2cost{{$item->id}}" style="display:none;" name="way2cost">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-info custom-btn" style="color: #efefef;
                background-color: #2f3542;
                border: 1px solid #2f3542;
                border-color: #2f3542;" data-dismiss="modal">Cancle</button>
                <button type="submit" class="btn btn-info custom-btn">Confirm</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endforeach
{{--  =======> Car Book Now Modal <=======  --}}

@endforeach






{{-- ============< Contact Part Starts >=========== --}}
<section class="contact-part" id="contact">
    <div class="container">
        <div class="row">
            <div class="col-md-12 m-t-70 m-b-70">
                <div class="contact-us" data-aos="zoom-in" data-aos-delay="100" data-aos-duration="1000" data-aos-easing="ease-in-out">
                    <div class="row">
                        <div class="col-md-4 address">
                            <div class="row">
                                <div class="col-md-12 m-b-25">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <i class="fas fa-home fa-2x"></i>
                                        </div>
                                        <div class="col-md-9">
                                            <h5 class="contact-header">contact address</h5>
                                            <p>Tajkia's Car Booking, Zindabazar, Sylhet, Bangladesh</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 m-b-25">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <i class="fas fa-phone fa-2x"></i>
                                        </div>
                                        <div class="col-md-9">
                                            <h5 class="contact-header">contact number</h5>
                                            <p>+880 1711111111 <br />+880 1722222222</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <i class="fas fa-envelope fa-2x"></i>
                                        </div>
                                        <div class="col-md-9">
                                            <h5 class="contact-header">mail address</h5>
                                            <p>email@gmail.com <br />email@gmail.com</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                        <form action="{{ Route('order.mail') }}" method="POST">
                            @csrf
                            <div class="row">
                                <div class="col-md-12"><h5 class="contact-header">contact form</h5></div>
                                <div class="col-md-12">
                                    <div class="fire_input1">
                                        <input type="text" placeholder="Name" name="name" class="input1">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="fire_input1">
                                        <input type="text" placeholder="Email" name="email" class="input1">
                                    </div>
                                </div>
                                <div class="col-md-12 textarea ">
                                    <div class="fire_input1">
                                        <textarea rows="3" placeholder="Message" name="msg" class="input1"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-info custom-btn btn-lg float-right">Send Message</button>
                                </div>
                            </div>
                        </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- =============< Contact Part Ends >============ --}}





{{-- ============< Part Starts >=========== --}}

{{-- =============< Part Ends >============ --}}


{{-- ============< Part Starts >=========== --}}

{{-- =============< Part Ends >============ --}}


{{-- ============< Part Starts >=========== --}}

{{-- =============< Part Ends >============ --}}


@endsection