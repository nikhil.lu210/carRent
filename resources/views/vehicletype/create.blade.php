@extends('layouts.app')

@section('content')
@foreach($errors->all() as $error)
    <ul>
        <li>{{ $error }}</li>
    </ul>
@endforeach
<section class="type-add-part">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form class="fire_input2" method="post" action="{{ route('admin.types.store')}}">
                @csrf
                    <div class="card">
                        <div class="card-header">
                            <div class="heading float-left">
                                <h2>Add New Vehicle Type</h2>
                            </div>
                            <div class="add-type float-right">
                                <a href="{{ route('admin.types') }}" class="btn btn-info custom-btn"><i class="fas fa-hand-point-left"></i> Back</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-custom-background">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="group">
                                            <label>Vehicle Type: </label>
                                            <input type="text" placeholder="ex: Car" name="vehicleType" class="input1">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="btn-group float-right" role="group" aria-label="Basic example">
                                        <button type="reset" class="btn btn-info custom-btn"><i class="far fa-thumbs-down"></i> Reset</button>
                                        <button type="submit" class="btn btn-info custom-btn"><i class="far fa-thumbs-up"></i> Add Type</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection