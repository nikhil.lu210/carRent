@extends('layouts.app')

@section('content')
<section class="car-list-part">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card text-center">
                    <div class="card-header">
                        <div class="heading float-left">
                            <h2>All Vehicle Type List</h2>
                        </div>
                        <div class="add-car float-right">
                            <a href="{{ route('admin.types.create') }}" class="btn btn-info custom-btn"><i class="fas fa-hand-point-right"></i> Add Type</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-dark table-custom">
                            <thead>
                                <tr>
                                    <th scope="col">SL.</th>
                                    <th scope="col">Vahicle Type</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($vehicals as  $key =>$vehicletype)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $vehicletype->name }}</td>
                                    <td>
                                        <div class="btn-group" role="group" aria-label="Basic example">
                                            <a href="{{ route('admin.types.destroy', ['id'=>$vehicletype->id])}}" class="btn btn-info custom-btn btn-sm">
                                                <i class="fas fa-trash"></i> 
                                                Delete
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection