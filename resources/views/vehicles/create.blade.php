@extends('layouts.app')

@section('stylesheet')
<style>
.nice-select {
    line-height: 35px;
    background-color: #1f2532;
    color: #fff;
    border: solid 1px #1f2532;
}
.nice-select:active, .nice-select.open, .nice-select:focus{
    border-color: #ffbe00;
}
.nice-select .list{
    width: 100%;
    background: #1f2532;
    color: #fff;
}
.nice-select .option:hover {
    background-color: #ffbe00;
    color: #fff;
}
</style>    
@endsection

@section('content')
@foreach($errors->all() as $error)
    <ul>
        <li>{{ $error }}</li>
    </ul>
@endforeach
<section class="car-add-part">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <form class="fire_input2" method="post" action="{{ route('admin.vehicle.store')}}" enctype="multipart/form-data">
                    @csrf
                        <div class="card">
                            <div class="card-header">
                                <div class="heading float-left">
                                    <h2>Add New Car</h2>
                                </div>
                                <div class="add-car float-right">
                                    <a href="{{ route('admin.vehicle') }}" class="btn btn-info custom-btn"><i class="fas fa-hand-point-left"></i> Back</a>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="form-custom-background">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="group">
                                                <label>Vehicle Type: </label>
                                                <select class="fire_select1 fire_select" name="type_id" required>
                                                    <option data-display="Select Type" >Select Type</option>
                                                    @foreach($types as $type)
                                                        <option value="{{ $type->id }}">{{ $type->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="group">
                                                <label>Vehicle name: </label>
                                                <input type="text" placeholder="ex: Toyota Camry" name="name" class="input1" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="group">
                                                <label>Seat number: </label>
                                                <input type="number" min="2" max="50" placeholder="ex: 05" name="seat_no" class="input1" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="group">
                                                <label>Reg: No: </label>
                                                <input type="text" max="6" placeholder="example-1234" name="reg_no" class="input1" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="group">
                                                <label>cost 1 way per Day: </label>
                                                <input type="number" min="500" max="90000" placeholder="input amount" name="cost_1_way" class="input1" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="group">
                                                <label>cost 2 way per Day: </label>
                                                <input type="number" min="500" max="90000" placeholder="input amount" name="cost_2_way" class="input1" required>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="group">
                                                <label>car Photo: </label>
                                                <input type="file" name="avatar" class="input1">
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="btn-group float-right" role="group" aria-label="Basic example">
                                            <button type="reset" class="btn btn-info custom-btn"><i class="far fa-thumbs-down"></i> Reset</button>
                                            <button type="submit" class="btn btn-info custom-btn"><i class="far fa-thumbs-up"></i> Add Car</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection