@extends('layouts.app')

@section('content')
<section class="car-list-part">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card text-center">
                    <div class="card-header">
                        <div class="heading float-left">
                            <h2>All Car List</h2>
                        </div>
                        <div class="add-car float-right">
                            <a href="{{ route('admin.vehicle.create') }}" class="btn btn-info custom-btn"><i class="fas fa-hand-point-right"></i> Add Car</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-dark table-custom">
                            <thead>
                                <tr>
                                    <th>sl</th>
                                    <th>image</th>
                                    <th scope="col">Vehicle Type</th>
                                    <th scope="col">Vahicle seat_no</th>
                                    <th scope="col">Vahicle reg_no</th>
                                    <th scope="col">Cost 1 Way</th>
                                    <th scope="col">Cost 2 Way</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($vehicles as $key=>$vehicle)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>
                                        <img src="{{ asset('vehicle_image/'.$vehicle->avatar)}}" alt="" style="max-height: 35px;max-weight:35px;">
                                    </td>
                                    <td>{{ App\Type::find($vehicle->type_id)->name }}</td>
                                    <td>{{ $vehicle->seat_no }}</td>
                                    <td>{{ $vehicle->reg_no }}</td>
                                    <td>{{ $vehicle->perdat_cost_1_way }}</td>
                                    <td>{{ $vehicle->perdat_cost_2_way }}</td>
                                    {{-- <td>{{ $vehicle->totalQuantity - $vehicle->bookedQuantity }}</td> --}}
                                    <td>
                                        <div class="btn-group" role="group" aria-label="Basic example">
                                            <a href="{{ route('admin.vehicle.destroy', ['id'=>$vehicle->id])}}" class="btn btn-info custom-btn btn-sm">
                                                <i class="fas fa-trash"></i> 
                                                Delete
                                            </a>

                                            {{-- <a href="{{ route('car.show', ['id'=>$car->id])}}" class="btn btn-info custom-btn btn-sm">
                                                <i class="fas fa-info-circle"></i> 
                                                Details
                                            </a> --}}
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection