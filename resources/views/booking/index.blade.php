@extends('layouts.app')

@section('content')
<section class="booking-list-part">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card text-center">
                    <div class="card-header">
                        <div class="heading float-left">
                            <h2>All Orders</h2>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-dark table-custom">
                            <thead>
                                <tr>
                                    <th scope="col">Client Name</th>
                                    <th scope="col">Mobile No.</th>
                                    <th scope="col">Vehicle Type</th>
                                    <th scope="col">Pickup Date</th>
                                    <th scope="col">pickup Point</th>
                                    <th scope="col">Drop Point</th>
                                    <th scope="col">Quantity</th>
                                    <th scope="col">t. Cost</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($orders as $order)
                                <tr>
                                    <td>{{ $order->user_name }}</td> 
                                    <td>{{ $order->mobile }}</td>
                                    <td>{{ App\Type::find($order->type_id)->name }}</td>
                                    <td>{{ $order->order_day }}</td>
                                    <td>{{ $order->pickPoint }}</td>
                                    <td>{{ $order->dropPoint }}</td>
                                    <td>{{ $order->needed_vahical }}</td>
                                    <td>{{ $order->total }}</td>
                                    <td>
                                        <div class="btn-group" role="group" aria-label="Basic example">
                                            <a href="{{ route('admin.orders.destroy', ['id' => $order->id]) }}" class="btn btn-info custom-btn btn-sm" onclick="return confirm('Are you sure want to delete?');">
                                                <i class="fas fa-trash"></i> 
                                                Delete
                                            </a>

                                            {{-- <a href="#" class="btn btn-info custom-btn btn-sm">
                                                <i class="fas fa-info-circle"></i> 
                                                Details
                                            </a> --}}
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection