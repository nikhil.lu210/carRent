@extends('layouts.app') 
@section('content') 
@foreach($errors->all() as $error)
<ul>
    <li>{{ $error }}</li>
</ul>
@endforeach
<section class="booking-show-part">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form class="fire_input2" method="post" action="{{route('booking.update',['id'=>$booking->id])}}">
                    @csrf
                    <div class="card">
                        <div class="card-header">
                            <div class="heading float-left">
                                <h2>Booking Details</h2>
                            </div>
                            <div class="add-booking float-right">
                                <a href="{{ route('bookings') }}" class="btn btn-info custom-btn"><i class="fas fa-hand-point-left"></i> Back</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-custom-background">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="group">
                                            <label>Client Name: </label>
                                            <input type="text" value="{{$booking->clientName}}" name="clientName" class="input1 removeDis" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="group">
                                            <label>Contact Number: </label>
                                            <input type="text" value="{{$booking->mobileNumber}}" name="mobileNumber" class="input1 removeDis" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="group">
                                            <label>Email Address: </label>
                                            <input type="text" value="{{$booking->emailAddress}}" name="emailAddress" class="input1 removeDis" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="group">
                                            <label>Pick Up Point: </label>
                                            <input type="text" value="{{$booking->pickPoint}}" name="pickPoint" class="input1 removeDis" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="group">
                                            <label>Dropping Point: </label>
                                            <input type="text" value="{{$booking->dropPoint}}" name="dropPoint" class="input1 removeDis" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="group">
                                            <label>Start Date: </label>
                                            <input type="text" value="{{$booking->startDate}}" name="startDate" class="input1 datepicker-here removeDis" data-language='en'  disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="group">
                                            <label>End Date: </label>
                                            <input type="text" value="{{$booking->endDate}}" name="endDate" class="input1 datepicker-here removeDis" data-language='en'  disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        {{--  <div class="group">
                                            <label>Vehicle Type: </label>
                                            <input type="text" value="{{$booking->vahicleType}}" name="vahicleType" class="input1 removeDis" disabled>
                                        </div>  --}}
                                        <div class="fire_select2 fire_select">
                                            <label>Vehicle Type: </label>
                                            <select name="vahicleType" class="removeDis" disabled>
                                                <option value="{{$booking->vahicleType}}">{{ $booking->vahicleType }}</option>
                                                <option value="Car">Car</option>
                                                <option value="Hiace">Hiace</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        {{--  <div class="group">
                                            <label>Vehicle Model: </label>
                                            <input type="text" value="{{$booking->vahicleModel}}" name="vahicleModel" class="input1 removeDis" disabled>
                                        </div>  --}}
                                        <div class="fire_select2 fire_select">
                                            <label>Vehicle Type: </label>
                                            <select name="vahicleModel" class="removeDis" disabled>
                                                <option value="{{$booking->vahicleModel}}">{{ $booking->vahicleModel }}</option>
                                                <option value="Car">Car</option>
                                                <option value="Hiace">Hiace</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="group">
                                            <label>Quantity: </label>
                                            <input type="text" value="{{$booking->vahicleQuantity}}" name="vahicleQuantity" class="input1 removeDis" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="btn-group float-right" role="group" aria-label="Basic example">
                                        <button type="button" class="btn btn-info custom-btn" id="removeDisabled"><i class="fas fa-pencil-alt"></i> Edit</button>

                                        <button type="button" class="btn btn-info custom-btn hiddenButton d-none" id="addDisabled"><i class="far fa-thumbs-down"></i> Cancel</button>

                                        <button type="submit" class="btn btn-info custom-btn hiddenButton d-none"><i class="far fa-thumbs-up"></i> Update</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection