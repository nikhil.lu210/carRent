/* ===========< Navbar Fixed Top Part Starts >========== */

// if (window.screen.width > 780) {
//   $(window).scroll(function(){
//       if ($(this).scrollTop() >= 100) {
//              $( "nav" ).addClass( "sticky-top" );
//         } else {
//              $( "nav" ).removeClass( "sticky-top" );
//         }
//     });
// }
/* ============< Navbar Fixed Top Part Ends >=========== */

/* ===========< Navbar Scrolling Animation Part Starts >========== */
// $(function(){
// 	var navbar = $('.navbar');

// 	$(window).scroll(function(){
// 		if($(window).scrollTop() <= 40){
// 			navbar.removeClass('navbar-scroll');
// 		} else {
// 			navbar.addClass('navbar-scroll');
// 		}
// 	});
// });
/* ============< Navbar Scrolling Animation Part Ends >=========== */

/* ===========< Price Part Starts >========== */
$(document).ready(function() {
  $("select.way").change(function() {
    var a = "input#way1cost"+this.id;
    var b = "input#way2cost"+this.id;
    var way1 = $(a).val();
    var way2 = $(b).val();
    var id ="select#"+ this.id;

    var way = $(id).val();
    // alert(total);
    b = "input#needed_vahicle" + this.id;
    a = $(b).val();

    var total =
      way == 1
        ? way1 * a
        : way2 * a;
    
    console.log(total);

    $(".total-cost").html(total);
  });
});
/* ============< Price Part Ends >=========== */

/* ===========< Scroll-Top Part Starts >========== */
$(document).ready(function() {
  /******************************
      BOTTOM SCROLL TOP BUTTON
   ******************************/

  // declare variable
  var scrollTop = $(".scrollTop");

  $(window).scroll(function() {
    // declare variable
    var topPos = $(this).scrollTop();

    // if user scrolls down - show scroll to top button
    if (topPos > 100) {
      $(scrollTop).css("opacity", "1");
    } else {
      $(scrollTop).css("opacity", "0");
    }
  }); // scroll END

  //Click event to scroll to top
  $(scrollTop).click(function() {
    $("html, body").animate(
      {
        scrollTop: 0
      },
      800
    );
    return false;
  });
});
/* ============< Scroll-Top Part Ends >=========== */

$(function() {
  $("#car1").hide();
  $("#hiace1").hide();

  $("#vehicleType").change(function() {
    if ($(this).val() == "car") {
      $("#car1").show();
      $("#hiace1").hide();
    }
    if ($(this).val() == "hiace") {
      $("#hiace1").show();
      $("#car1").hide();
    }
  });
});

// Smooth Scroll
$(document).ready(function() {
  // Add smooth scrolling to all links
  $("a").on("click", function(event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $("html, body").animate(
        {
          scrollTop: $(hash).offset().top
        },
        800,
        function() {
          // Add hash (#) to URL when done scrolling (default click behavior)
          window.location.hash = hash;
        }
      );
    } // End if
  });
});
